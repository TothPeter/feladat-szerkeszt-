//#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cctype>

using namespace std;

void read_spaces(istream &is);
int read_integer(istream &is);
int read_integer(istream &corr_out, istream &out);
void endline(istream& out);
void nothing_else(istream& out);

struct FormatErrorException : public exception {
   virtual const char * what () const throw () {
      return "Rossz formátum";
   }
};

struct BadResultErrorException : public exception {
   virtual const char * what () const throw () {
      return "Hibás eredmény";
   }
};

void read_spaces(istream &is) {
    char c = is.peek();
    while (c == ' ' || c == '\t') {
        is.get();
        c = is.peek();
    }
    if (c == '\n') {
        FormatErrorException e;
        throw e;
    }
}

int read_integer(istream &is) {
    read_spaces(is);
    
    int result;
    is >> result;

    if (is.fail()) {
        FormatErrorException e;
        throw e;
    }

    return result;
}

int read_integer(istream &corr_out, istream &out) {
    read_spaces(out);
    
    int corr_result, result;
    corr_out >> corr_result;
    out >> result;

    if (out.fail()) {
        FormatErrorException e;
        throw e;
    }

    if (corr_result != result) {
        BadResultErrorException e;
        throw e;
    }

    return corr_result;
}

void endline(istream& out) {
    char c;
    while (!out.eof()) {
        c = out.get();
        if (!out.eof()) {
            if (c == '\n') {
                return;
            }
            if (!isspace(c)) {
                FormatErrorException e;
                throw e;
            }
        }
    }
}


void nothing_else(istream& out) {
    char c;
    while (!out.eof()) {
        c = out.get();
        if (!out.eof() && !isspace(c)) {
            FormatErrorException e;
            throw e;
        }
    }
}

const int ROSSZ_INT = -199;
const string ROSSZ_STR = "__-199__";

string stringJoin(vector<string> v, string separator);
void msg(string Tes, int resz, int pont, string verdikt, bool vege);
string reverseString(string s);
void StringExplode(string str, string separator, vector<string> &results);
void StringExplode(string str, string separator, vector<int> &results);
void StringExplodeAndSort(string str, string separator, vector<string> &results);
void StringExplodeAndSort(string str, string separator, vector<int> &results);
string StringExplodeAndJoin(string str, string separator);
string StringExplodeAndSortAndJoin(string str, string separator);
string StringExplodeAndSortAndJoinInt(string str, string separator);
string Trim(string s, const string drop = " ");
bool isSameFile(ifstream &correctTestresult_file, ifstream &testresult_file);
//template<typename T>
    vector<int> readFromStreamIntoVector(istream &f);
bool readOneIntFromLine(istream &f, int &i);

class HibaKezelo {
private:
    int reszFeladatokSzama;
    string Tes;
    vector<string> hibak;

public:
    HibaKezelo(int db, string tes) {
        reszFeladatokSzama = db;
        Tes = tes;
        for (int i = 0; i < db; ++i)
        {
            stringstream ss;
            ss << Tes << ";" << i+1 << ";" << 0 << ";" << "Hiba az előző részfeladatok valamelyikében";
            hibak.push_back(ss.str());
        }
    }
    ~HibaKezelo() {
        cout << stringJoin(hibak, ":") << endl;
    }
    void msg(int resz, int pont, string verdikt, bool vege) {
        stringstream result;
        result << Tes << ";" << resz << ";" << pont << ";" << verdikt;
        hibak[resz-1] = result.str();
        if (vege) {
            throw 1;
        }
    }
};

string stringJoin(vector<string> v, string separator) {
    stringstream result;

    int size = v.size();

    for (int i = 0; i < size-1; i++) {
        result << v[i] << separator;
    }
    if (size>0) {
        result << v[size-1];
    }

    return result.str();
}

string reverseString(string s) {
    reverse(s.begin(), s.end());
    return s;
}

string StringExplodeAndJoin(string str, string separator) {
    vector<string> v;
    stringstream result;

    StringExplode(str, separator, v);

    int size = v.size();

    for (int i = 0; i < size-1; i++) {
        result << v[i] << separator;
    }
    if (size>0) {
        result << v[size-1];
    }

    return result.str();
}

string StringExplodeAndSortAndJoin(string str, string separator) {
    vector<string> v;
    stringstream result;

    StringExplodeAndSort(str, separator, v);

    int size = v.size();

    for (int i = 0; i < size-1; i++) {
        result << v[i] << separator;
    }
    if (size>0) {
        result << v[size-1];
    }

    return result.str();
}

string StringExplodeAndSortAndJoinInt(string str, string separator) {
    vector<int> v;
    stringstream result;

    StringExplodeAndSort(str, separator, v);

    int size = v.size();

    for (int i = 0; i < size-1; i++) {
        result << v[i] << separator;
    }
    if (size>0) {
        result << v[size-1];
    }

    return result.str();
}

void StringExplodeAndSort(string str, string separator, vector<string> &results) {
    StringExplode(str, separator, results);
    sort(results.begin(), results.end());
}

void StringExplodeAndSort(string str, string separator, vector<int> &results) {
    StringExplode(str, separator, results);
    sort(results.begin(), results.end());
}

void StringExplode(string str, string separator, vector<string> &results) {
    int found;
    found = str.find_first_of(separator);
    while (found != string::npos) {
        if (found > 0) {
            results.push_back(str.substr(0, found));
        }
        str = str.substr(found + 1);
        found = str.find_first_of(separator);
    }
    if (str.length() > 0) {
        results.push_back(str);
    }
}

void StringExplode(string str, string separator, vector<int> &results) {
    int found;
    found = str.find_first_of(separator);
    while (found != string::npos) {
        if (found > 0) {
            int number = atoi(str.substr(0, found).c_str());
            results.push_back( number );
        }
        str = str.substr(found + 1);
        found = str.find_first_of(separator);
    }
    if (str.length() > 0) {
        int number = atoi(str.c_str());
        results.push_back( number );
    }
}

string Trim(string s, const string drop)
{
    string r=s.erase(s.find_last_not_of(drop)+1);
    return r.erase(0,r.find_first_not_of(drop));
}

bool isSameFile(ifstream &correctTestresult_file, ifstream &testresult_file) {
    vector<string> corrv, v;
    string s;
    while (!correctTestresult_file.eof()) {
        getline(correctTestresult_file, s);
        if (!correctTestresult_file.fail()) {
            s = Trim(s, " ");
            corrv.push_back(s);
        }
    }
    while (corrv.size() > 0 && corrv.back().length() == 0) {
        corrv.pop_back();
    }

    while (!testresult_file.eof()) {
        getline(testresult_file, s);
        if (!testresult_file.fail()) {
            s = Trim(s, " ");
            v.push_back(s);
        }
    }
    while (v.size() > 0 && v.back().length() == 0) {
        v.pop_back();
    }

    if (corrv.size() != v.size()) {
        return false;
    }

    int n = corrv.size();
    for (int i = 0; i<n; i++) {
        if (corrv[i] != v[i]) {
            return false;
        }
    }

    return true;
}

//template<typename T>
vector<int> readFromStreamIntoVector(istream &f, const int ROSSZ) {
    vector<int> v;
    int sv = ROSSZ;
    f >> sv;
    while (sv != ROSSZ) {
        v.push_back(sv);
        sv = ROSSZ;
        f >> sv;
    }
    return v;
}

vector<string> readFromStreamIntoVector(istream &f, const string ROSSZ) {
    vector<string> v;
    string sv = ROSSZ;
    f >> sv;
    while (sv != ROSSZ) {
        v.push_back(sv);
        sv = ROSSZ;
        f >> sv;
    }
    return v;
}

bool readOneIntFromLine(istream &f, int &i) {
    string sv = "";
    getline(f, sv);
    sv = Trim(sv);
    stringstream ss(sv);
    ss >> i; 
    bool rossz = ss.fail();
    // cout << i << endl;
    string mar;
    getline(ss, mar);
    // cout << "q" << mar << "q" << endl;
    
    return !(rossz || mar != "");
}
