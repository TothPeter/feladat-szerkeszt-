#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <vector>

#include "common.h"


// #define maxN 10001
// #define maxT 10001

using namespace std;

int main(int argc, char *argv[]){
//argv[1]: a teszt adatok könyvtára
//argv[2]: a versenyző kimeneteit tartalmazó könyvtár
//argv[3]: a teszteset sorszáma

    string Tes=argv[3];

    string TinFn=(string)argv[1]+"/in."+Tes;
    string ToutFn=(string)argv[1]+"/out."+Tes;
    string VoutFn=(string)argv[2]+"/out."+Tes;

    ifstream in(TinFn.c_str());
    ifstream corr_out(ToutFn.c_str());
    ifstream out(VoutFn.c_str());

    HibaKezelo hk(1, Tes);  //részfeladatok száma, teszteset száma

    try {

        try {
            read_integer(corr_out, out);
            nothing_else(out);
            hk.msg(1, 1, "Helyes", true); 
        }
        catch (FormatErrorException& e) {
            hk.msg(1, 0, e.what(), true);
        }
        catch (BadResultErrorException& e) {
            hk.msg(1, 0, e.what(), true);
        }

    }
    catch (int kod) {}

    in.close();
    corr_out.close();
    out.close();

    return 0;
}
