import { Component, OnInit, Input } from '@angular/core';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload';
import { Task } from '../task';
import { TaskService } from '../task.service';
import { HomeComponent } from '../home/home.component';

//peti baszas-----------------------------------------------
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'
import { FileService } from '../file.service';
//----------------------------------------------------------


@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {

  rootMapName=HomeComponent.newTaskLocation;
  isTest=false;
  fileType='md';
  descriptions:String[];
	constructor(private http: HttpClient, private taskService:TaskService, private fileService:FileService) {
    if(this.rootMapName===''){
      this.rootMapName=this.determineRootFolderName();
    }
    
	 }
	  
	  async ngOnInit() {
     // this.descriptions=await this.fileService.getFilesInADirectory(this.rootMapName, 'description');
  }
  
  determineRootFolderName():string{
    //console.log(this.taskService);
   var numOfTasks=this.taskService.getTasksSize();
   let serialNumber=(numOfTasks+1).toString();
     while(serialNumber.length<9){
         serialNumber='0'+serialNumber;
     }
     return serialNumber;
  }


}
