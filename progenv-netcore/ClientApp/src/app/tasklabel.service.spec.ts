import { TestBed } from '@angular/core/testing';

import { TasklabelService } from './tasklabel.service';

describe('TasklabelService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TasklabelService = TestBed.get(TasklabelService);
    expect(service).toBeTruthy();
  });
});
