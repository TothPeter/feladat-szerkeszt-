import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Task } from '../task';
import { HomeComponent } from '../home/home.component';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-add-task-pdf',
  templateUrl: './add-task-pdf.component.html',
  styleUrls: ['./add-task-pdf.component.css']
})
export class AddTaskPdfComponent implements OnInit {
public allTasks: Task[];
  rootMapName=HomeComponent.newTaskLocation;
isTest=false;
fileType='pdf';
  constructor(private taskService:TaskService) {
    if(this.rootMapName===''){
      this.rootMapName=this.determineRootFolderName();
    }}

	async ngOnInit() {
    this.allTasks = await this.taskService.getTasks();
    console.log(this.taskService.getTasksSize());
  }
  determineRootFolderName():string{
    
    var numOfTasks=this.taskService.getTasksSize();
    let serialNumber=(numOfTasks+1).toString();
      while(serialNumber.length<9){
          serialNumber='0'+serialNumber;
      }
      return serialNumber;
   }
}
