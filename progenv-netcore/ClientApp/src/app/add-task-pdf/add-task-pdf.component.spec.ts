import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTaskPdfComponent } from './add-task-pdf.component';

describe('AddTaskPdfComponent', () => {
  let component: AddTaskPdfComponent;
  let fixture: ComponentFixture<AddTaskPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTaskPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTaskPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
