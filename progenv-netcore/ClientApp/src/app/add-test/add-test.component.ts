import { Component, OnInit } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-add-test',
  templateUrl: './add-test.component.html',
  styleUrls: ['./add-test.component.css']
})
export class AddTestComponent implements OnInit {
  rootMapName=HomeComponent.newTaskLocation;
isTest=true;
inFileType='in';
outFileType='out';
  constructor(private taskService:TaskService) { 
    if(this.rootMapName===''){
      this.rootMapName=this.determineRootFolderName();
    }}

  ngOnInit() {
  }
  determineRootFolderName():string{
    
    var numOfTasks=this.taskService.getTasksSize();
    let serialNumber=(numOfTasks+1).toString();
      while(serialNumber.length<9){
          serialNumber='0'+serialNumber;
      }
      return serialNumber;
   }
}
