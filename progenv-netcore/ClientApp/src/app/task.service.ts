import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Task } from './task';
import { httpOptions } from './auth.service';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private taskUrl = 'http://localhost:5000/task';

    constructor(private http: HttpClient) { }
    getTasks(): Promise<Task[]> {
      return this.http.get<Task[]>(
        this.taskUrl,
        httpOptions
      ).toPromise();
  }
  getTasksSize():number{
    var tasks;
    var size:number;
    this.http.get<Task[]>(
      this.taskUrl,
      httpOptions
    ).subscribe((data:Task[])=>{
      tasks=data;
    },null,()=>size=this.determineSize(tasks));
    return size;
  }
  private determineSize(tasks:Task[]):number{
    return tasks.length;
  }
  getTaskById(id:number):  Promise<Task>{
    
    return this.http.get<Task>(
      `${this.taskUrl}/${id}`,
      httpOptions
    ).toPromise();

}
getTasksByTextAndType(text:string, type:string):Observable<Task[]>{
  text=text.trim();
  type=type.trim();
 /* const options={
    headers:new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    }),
    params: new HttpParams().set('text', term)};*/
    var textParam='nosearchtext';
    if(text!='nosearchtext'){
      var httpParam=new HttpParams().set('text', text);
      textParam=httpParam.get('text');
    }
    var typeParam='nosearchtype';
    if(type!='nosearchtype'){
       httpParam=new HttpParams().set('type', type);
       typeParam=httpParam.get('type');
    }
    

  return this.http.get<Task[]>(
    this.taskUrl+'/searchtype='+typeParam+'&searchtext='+textParam
  );
}
}
