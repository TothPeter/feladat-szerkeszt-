import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Label } from './label';
import { httpOptions } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class LabelService {
  private labelUrl = 'http://localhost:5000/label';
  constructor(private http: HttpClient) { }
  getLabels(): Promise<Label[]> {
    return this.http.get<Label[]>(
      this.labelUrl,
      httpOptions
    ).toPromise();
  }
}
