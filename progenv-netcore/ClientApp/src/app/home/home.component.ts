import { Component, Inject, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Task } from '../task';
import { Label } from '../label';
import { TaskService } from '../task.service';
import { LabelService } from '../label.service';
import { Router } from '@angular/router';
import { TaskLabel } from '../tasklabel';
import { TasklabelService } from '../tasklabel.service';

//import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})




export class HomeComponent {
  public allTasks: Task[];
  public actuallySeenTasks: Task[];
  public filteredTasks:Array<Task>;
  public labels: Set<Label>;
  private lastMaxTaskIndex = 0;
  static newTaskLocation:string;
  selectedType='nosearchtype';
  selectedText='nosearchtext';
 /* taskForm = this.fb.group({
    taskType: ['']
   });
   get taskType() { return this.taskForm.get('taskType'); }*/
   @Output() save = new EventEmitter<Task>();
   constructor(private taskService:TaskService, private labelService:LabelService, private taskLabelService:TasklabelService, private router: Router
   ){
  }

async ngOnInit() {
  this.allTasks = await this.taskService.getTasks();
  this.labels=new Set(await this.labelService.getLabels());
  this.getNextNTask(false);
}

async createNewTask(){
   let serialNumber=(this.allTasks.length+1).toString();
   while(serialNumber.length<9){
       serialNumber='0'+serialNumber;
   }
    HomeComponent.newTaskLocation=serialNumber;
    this.router.navigate(['/addtask']);
}

getNextNTask(filteredData:boolean){
  var startIndex=0;
  var endIndex=10;
  if(this.lastMaxTaskIndex!=0){
    startIndex=this.lastMaxTaskIndex-10;
    endIndex=this.lastMaxTaskIndex;
    this.lastMaxTaskIndex+=10;
  }else{
    this.lastMaxTaskIndex=20;
  }
  if(!filteredData && endIndex>=this.allTasks.length){
    endIndex=this.allTasks.length;
  }else if(filteredData && endIndex>=this.filteredTasks.length){
    endIndex=this.filteredTasks.length;
  }
  var selectTasksFrom=this.allTasks;
  if(filteredData){
    selectTasksFrom=this.filteredTasks;
  }
  this.actuallySeenTasks=new Array(endIndex-startIndex);
  for(let i=startIndex; i<endIndex; ++i){
    this.actuallySeenTasks[i]=selectTasksFrom[i];  
  }
}

onSearch() {

  
  var selectedOptions= (<HTMLSelectElement>document.getElementById("typesearch")).selectedOptions;
  this.selectedType=selectedOptions[0].label;
  if(this.selectedType === 'Összes típus'){
      this.selectedType ='nosearchtype';
    }
    this.selectedText =   (<HTMLInputElement>document.getElementById("textSearch")).value;

    if(this.selectedText===''){
  this.selectedText='nosearchtext';
  
    }
   
    if(this.selectedText!='nosearchtext'||this.selectedType!='nosearchtype'){
    this.taskService.getTasksByTextAndType(this.selectedText, this.selectedType).subscribe((data:Task[])=>{
      this.filteredTasks=data;
    },null,()=>this.searchCompleted());
  }else{
    this.lastMaxTaskIndex=0;
    this.filteredTasks=this.allTasks;
    this.getNextNTask(false);
  }
  
  }
  searchCompleted(){
  
    if(this.filteredTasks.length==0){
      alert("Nincs a keresésnek megfelelő elem!");
      this.lastMaxTaskIndex=0;
      this.getNextNTask(true);
    }else{
      this.lastMaxTaskIndex=0;
   this.getNextNTask(true);
    }
  }



}

// http://localhost:5000/task/searchtype=nosearchtype&searchtext=nosearchtext
