import { Component, OnInit, Input } from '@angular/core';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-file-hierarchy',
  templateUrl: './file-hierarchy.component.html',
  styleUrls: ['./file-hierarchy.component.css']
})
export class FileHierarchyComponent implements OnInit {
hierarchy = document.getElementById("hierarchy");
  @Input('newTaskLocation') newTaskLocation=HomeComponent.newTaskLocation;
  @Input('descriptions') descriptions:String[];
  constructor() {

  }
	ngOnInit() {
    var hierarchy = document.getElementById("hierarchy");
    hierarchy.addEventListener("click", function(event){
        var elem = event.target as HTMLElement;
        if(elem.tagName.toLowerCase() == "span" && elem !== event.currentTarget)
        {
            var type = elem.classList.contains("folder") ? "folder" : "file";
            if(type=="file")
            {
                alert("File accessed");
            }
            if(type=="folder")
            {
                var isexpanded = elem.dataset.isexpanded=="true";
                if(isexpanded)
                {
                    elem.classList.remove("fa-folder-o");
                    elem.classList.add("fa-folder");
                }
                else
                {
                    elem.classList.remove("fa-folder");
                    elem.classList.add("fa-folder-o");
                }
                if(isexpanded===true){
                  elem.dataset.isexpanded = "false";
                }else{
                  elem.dataset.isexpanded = "true";
                }
    
                var toggleelems = [].slice.call(elem.parentElement.children);
                var classnames = "file,foldercontainer,noitems".split(",");
    
                toggleelems.forEach(function(element){
                    if(classnames.some(function(val){return element.classList.contains(val);}))
                    element.style.display = isexpanded ? "none":"block";
                });
            }
        }
    });
  }





 
}
