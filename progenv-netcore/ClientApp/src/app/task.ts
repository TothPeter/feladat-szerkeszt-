export class Task{
    id=null;
    location:string='';
    mester_id:string='';
    uuid: string="";
    friendly_id:string='';
    title:string='';
    text_type:string='';
    text:string='';
    description_data:string='';
    difficulty:number=0;
    interactive:boolean=false;
    number_of_tests:number=0;
    number_of_subtests:number=0;
    sum_points:number=0;
    memory_limit:number=0;
    time_limit:number=0;
    created_at:string='';
    updated_at:string='';
}