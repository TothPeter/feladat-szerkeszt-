import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TaskLabel } from './tasklabel';
import { httpOptions } from './auth.service';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TasklabelService {
  private taskLabelUrl = 'http://localhost:5000/tasklabel';

  constructor(private http: HttpClient) { }
  getTaskLabels(): Promise<TaskLabel[]> {
    return this.http.get<TaskLabel[]>(
      this.taskLabelUrl,
      httpOptions
    ).toPromise();
}
getTaskLabelByLabelId(id:number):  Promise<TaskLabel[]>{
  return this.http.get<TaskLabel[]>(
    `${this.taskLabelUrl}/${id}`,
    httpOptions
  ).toPromise();


}
}
