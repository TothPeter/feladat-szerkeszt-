import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JsonInfosComponent } from './json-infos.component';

describe('JsonInfosComponent', () => {
  let component: JsonInfosComponent;
  let fixture: ComponentFixture<JsonInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JsonInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JsonInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
