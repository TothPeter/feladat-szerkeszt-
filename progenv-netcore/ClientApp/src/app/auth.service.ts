import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export const httpOptions={
  headers:new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Credentials': 'true',
  'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
})
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
}
