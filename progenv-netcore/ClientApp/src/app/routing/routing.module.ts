import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { AddTaskComponent } from '../add-task/add-task.component';
import { AddTaskPdfComponent } from '../add-task-pdf/add-task-pdf.component';
import { JsonInfosComponent } from '../json-infos/json-infos.component';
import { AddTestComponent } from '../add-test/add-test.component';
import { AddSolutionComponent } from '../add-solution/add-solution.component';
const routes: Routes = [
	{
		path: '',
		redirectTo: '/tasks',
		pathMatch: 'full'
	},
	{
		path: 'tasks',
		component: HomeComponent
	},
	{
		path: 'addtask',
		component: AddTaskComponent
	},
	{
		path: 'addtaskpdf',
    component: AddTaskPdfComponent
	},
{
    path: 'addjson',
    component: JsonInfosComponent
    },
    {
        path: 'addtest',
        component: AddTestComponent
    },
    {
        path: 'addsolution',
        component: AddSolutionComponent
    }
];
@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	declarations: []
})
export class RoutingModule { }
