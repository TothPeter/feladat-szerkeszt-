import { Injectable } from '@angular/core';
import { httpOptions } from './auth.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  private fileUrl = 'http://localhost:5000/file';
  constructor(private http: HttpClient) { }
  getFilesInADirectory(location:string, subFolderName:string): Promise<String[]> {
    return this.http.get<string[]>(
      this.fileUrl+'/'+location+'/fold/'+subFolderName,
      httpOptions
    ).toPromise();
  }
}
