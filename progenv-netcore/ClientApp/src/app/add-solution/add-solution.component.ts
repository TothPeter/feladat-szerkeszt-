import { Component, OnInit } from '@angular/core';
import { HomeComponent } from '../home/home.component';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-add-solution',
  templateUrl: './add-solution.component.html',
  styleUrls: ['./add-solution.component.css']
})
export class AddSolutionComponent implements OnInit {
  rootMapName=HomeComponent.newTaskLocation;
isTest=false;
fileType='cpp';
  constructor(private taskService:TaskService) {
    if(this.rootMapName===''){
      this.rootMapName=this.determineRootFolderName();
    }}
  

  ngOnInit() {
  }
  determineRootFolderName():string{
    
    var numOfTasks=this.taskService.getTasksSize();
    let serialNumber=(numOfTasks+1).toString();
      while(serialNumber.length<9){
          serialNumber='0'+serialNumber;
      }
      return serialNumber;
   }
}
