import { Component, Input } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http'
import { TaskService } from '../task.service';
import { httpOptions } from '../auth.service';

@Component({
  selector: 'app-add-upload',
  templateUrl: './add-upload.component.html'
})
export class AddUploadComponent {
  @Input('rootMapName') rootMapName = '';
  @Input('isTest') isTest=false;
  @Input ('fileType') fileType='md';
  public progress: number;
  public message: string;
  constructor(private http: HttpClient, private taskService:TaskService) {
    if(this.rootMapName===''){
      this.rootMapName=this.createNextFolderName();
    }
   }
 createNextFolderName():string{
  var numOfTasks=this.taskService.getTasksSize();
  let serialNumber=(numOfTasks+1).toString();
		while(serialNumber.length<9){
			  serialNumber='0'+serialNumber;
    }
    return serialNumber;
 }
  public upload(files:Array<File>) {
    if (files.length === 0)
      return;

    const formData = new FormData();

    var fileExtension;

    for (let file of files){
      formData.append(file.name, file);
      fileExtension= file.name;
      console.log("file.name: "+file.name);
    }
    console.log(this.fileType);
    if(this.fileType != 'out'&& this.fileType != 'in' &&this.fileType != fileExtension.split('.').slice(-1)[0].toLowerCase()){
      this.message = "Hibás fájl formátum!";
      return;
    }
    //console.log(this.fileType);

    if(this.rootMapName===''){
      this.rootMapName=this.createNextFolderName();
    }
    
    // /file/000000001/pdf
    // /file/000000001/md
    // /file/000000002/json
    // /file/000000002/test/in.5
    var url=`/file/`+this.rootMapName;
    if(this.isTest){
      url+=`/test`;
      var ind=0;
      this.http.get<number>(
        url+'/count/'+this.fileType,
        httpOptions
      ).subscribe((data:number)=>{
        ind=data;
      });
      if(ind==-1){
        this.fileType=this.fileType+'.'+ind;
      }else{
        this.fileType=this.fileType+'.'+(ind+1);
      }
    }
    
    url+=`/`+this.fileType;
    console.log('location: '+url);
    const uploadReq = new HttpRequest('POST', url, formData, {
      reportProgress: true,
    });

    this.http.request(uploadReq).subscribe(event => {
      this.message = "";
      if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
      else if (event.type === HttpEventType.Response)
        if (event.ok) {
          this.message = "A feltöltés sikeres volt!";
        }
        else
        {
          this.message = "Hiba történt a fájl feltöltésekor, hiba kód: " + event.status;
        }
    });
  }
}
