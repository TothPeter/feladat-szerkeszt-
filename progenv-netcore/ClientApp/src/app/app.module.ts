import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FileSelectDirective } from 'ng2-file-upload';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule, 
  MatButtonModule, 
  MatIconModule,
  MatListModule,
  MatButtonToggleModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule
} from '@angular/material';
import { AddTaskComponent } from './add-task/add-task.component';
import { RoutingModule } from './routing/routing.module';
import { AddTaskPdfComponent } from './add-task-pdf/add-task-pdf.component';
import { AddUploadComponent } from './add-upload/add-upload.component';
import { NavigatorComponent } from './navigator/navigator.component';
import { JsonInfosComponent } from './json-infos/json-infos.component';
import { AddTestComponent } from './add-test/add-test.component';
import { AddSolutionComponent } from './add-solution/add-solution.component';
import { FileHierarchyComponent } from './file-hierarchy/file-hierarchy.component';


@NgModule({
  declarations: [
		AppComponent,
		FileSelectDirective,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    AddTaskComponent,
    AddTaskPdfComponent,
    AddUploadComponent,
    NavigatorComponent,
    JsonInfosComponent,
    AddTestComponent,
    AddSolutionComponent,
    FileHierarchyComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'upload', component: AddUploadComponent },
    ]),
    BrowserAnimationsModule,
    MatToolbarModule, 
  MatButtonModule, 
  MatIconModule,
  MatListModule,
  MatButtonToggleModule,
  MatFormFieldModule,
  MatInputModule,
	MatSelectModule,
	RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
