using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AngularTaskManager.Models
{
    public class LabelDTO
    {
        [Key]
        public Int32 id { get; set; }
        public String text { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    public class TaskDTO   //kulon lehessen titlere  osszeg legyen pl 1, kereses 3, ...   ,     szovegre
    {
        [Key]
        public Int32 id { get; set; }
        public String friendly_id { get; set; }
        public String title { get; set; } // map from string
        public Int32 difficulty { get; set; }
        public Int32 memory_limit { get; set; }
        public Double time_limit { get; set; }
    }

    public class DirDTO
    {
        public String location { get; set; }
        public Boolean json { get; set; }
        public Boolean md { get; set; }
        public Boolean pdf { get; set; }
        public Int32 length { get; set; }
        public TaskDTO taskDTO { get; set; }
    }
}

