﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AngularTaskManager.Models
{
    public class MesterMigration
    {
        [Key]
        public Int32 id { get; set; }
        public Int32 mester_id { get; set; }
        public String last_run_hash { get; set; }
        public String created_at { get; set; }
        public String updated_at { get; set; }
    }
}
