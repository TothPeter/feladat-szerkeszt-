﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.FileProviders;


namespace AngularTaskManager.Models
{
    public class MyService : IService
    {
        private readonly MyDbContext _context;

        public MyService(MyDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Task> Tasks => _context.tasks;
        public IEnumerable<TaskLabel> TasksLabels => _context.x_tasks_labels;
        public IEnumerable<Label> Labels => _context.labels;
        public IEnumerable<User> Users => _context.users;
        public IEnumerable<DirDTO> Locations => GetDirectoryInfo();


        // public Task GetTask(Int32 id)
        // {
        //     return _context.tasks.Where(t => t.id == id).FirstOrDefault();
        // }

        public void AddTask(Task task)
        {
            _context.tasks.Add(task);
            _context.SaveChanges();
        }

        public void UpdateTask(Task task)
        {
            _context.tasks.Update(task);
            _context.SaveChanges();
        }

        public void RemoveTask(Task task)
        {
            _context.tasks.Remove(task);
            _context.SaveChanges();
        }

        public IEnumerable<DirDTO> GetDirectoryInfo()
        {
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            IDirectoryContents rootDirectoryContents = rootFileProvider.GetDirectoryContents("tasks");
            if (!rootDirectoryContents.Exists)
                return null;

            List<DirDTO> dirs = new List<DirDTO>();
            foreach (IFileInfo content in rootDirectoryContents)
            {
                PhysicalFileProvider subFileProvider = new PhysicalFileProvider(content.PhysicalPath);
                IFileInfo pdfInfo = subFileProvider.GetFileInfo("description/feladat.pdf");
                IFileInfo mdInfo = subFileProvider.GetFileInfo("description/feladat.md");
                IFileInfo jsonInfo = subFileProvider.GetFileInfo("description/feladat.txt.json");
                IDirectoryContents testsContents = subFileProvider.GetDirectoryContents("tests");

                // String feladatJson = System.IO.File.ReadAllText(content.PhysicalPath + "description/feladat.txt.json");
                // Console.WriteLine(feladatJson);

                dirs.Add(new DirDTO
                {
                    location = content.Name,
                    json = jsonInfo.Exists,
                    md = mdInfo.Exists,
                    pdf = pdfInfo.Exists,
                    length = testsContents.Count() / 2,
                    taskDTO = { }
                });
            }
            // var fileInfo = provider.GetFileInfo("Dockerfile");
            // Console.WriteLine(fileInfo.PhysicalPath);
            // Console.WriteLine(fileInfo.Name);
            // Console.WriteLine(fileInfo.Exists);
            return dirs.AsEnumerable();

        }
     public int GetTestFileNumberByLocationAndType(String location, String testFileType)
        {
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            IDirectoryContents rootDirectoryContents = rootFileProvider.GetDirectoryContents("tasks");
            if (!rootDirectoryContents.Exists)
                return -1;
            int counter=0;
            try
            {
                foreach (IFileInfo content in rootDirectoryContents)
                {
                    if (content.Name == location)
                    {
                        String[] fileNames=Directory.GetFiles(content.PhysicalPath + "/tests", "*ProfileHandler.cs", SearchOption.AllDirectories);
                        foreach(String name in fileNames){
                        if (name.Contains(testFileType)){
                            counter+=1;
                        }
                        }
                        
                    }
                }
                return counter;
            }catch{
                return -1;
            }
        }
        public String[] GetFileNamesInSubFolder(String location, String subFolderName){
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            IDirectoryContents rootDirectoryContents = rootFileProvider.GetDirectoryContents("tasks");
            if (!rootDirectoryContents.Exists)
                return null;
                try
            {
                HashSet<String> result=new HashSet<String>();
                foreach (IFileInfo content in rootDirectoryContents)
                {
                    if (content.Name == location)
                    {
                        String[] fileNames=Directory.GetFiles(content.PhysicalPath + "/"+subFolderName);
                        foreach(String name in fileNames){
                        result.Add(Path.GetFileName(name));
                   
                        }
                        }
                        
                    
                }
            return result.ToArray();
            }catch{
                return null;
            }
        }
        public String GetFileByLocation(String location, String fileType)
        {
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            IDirectoryContents rootDirectoryContents = rootFileProvider.GetDirectoryContents("tasks");
            if (!rootDirectoryContents.Exists)
                return null;

            try
            {
                foreach (IFileInfo content in rootDirectoryContents)
                {
                    if (content.Name == location)
                    {
                        if (fileType == "pdf")
                            return System.IO.File.ReadAllText(content.PhysicalPath + "/description/feladat.pdf");

                        else if (fileType == "md")
                            return System.IO.File.ReadAllText(content.PhysicalPath + "/description/feladat.md");

                        else if (fileType == "json")
                            return System.IO.File.ReadAllText(content.PhysicalPath + "/description/feladat.txt.json");
                        else if (fileType.Contains("in")||fileType.Contains("out")){
                            return System.IO.File.ReadAllText(content.PhysicalPath + "/tests/"+fileType);
                        }
                    }
                }

                return "";
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.ToString());
                return "";
            }
        }

        public String GetTestByLocation(String location, String testId)
        {
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            IDirectoryContents rootDirectoryContents = rootFileProvider.GetDirectoryContents("tasks");
            if (!rootDirectoryContents.Exists)
                return null;

            try
            {
                foreach (IFileInfo content in rootDirectoryContents)
                {
                    if (content.Name == location)
                    {
                        return System.IO.File.ReadAllText(content.PhysicalPath + "/tests/" + testId);
                    }
                }

                return "";
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine(e.ToString());
                return "";
            }
        }

        public void CreateFile2Location(String location, String fileType, IFormFile formFile)
        {
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            String physicalLocation = rootFileProvider.GetFileInfo("tasks").PhysicalPath + "/" + location;

            if (!rootFileProvider.GetDirectoryContents("tasks/" + location).Exists)
                Directory.CreateDirectory(physicalLocation  + "/description");

            if (formFile.Length > 0)
            {
                if (fileType == "pdf")
                {
                    using (var fileStream = new FileStream(physicalLocation + "/description/feladat.pdf", FileMode.Create))
                    {
                        formFile.CopyTo(fileStream);
                    }
                }
                else if (fileType == "md")
                {
                    using (var fileStream = new FileStream(physicalLocation + "/description/feladat.md", FileMode.Create))
                    {
                        formFile.CopyTo(fileStream);
                    }
                }
                else if (fileType == "json")
                {
                    using (var fileStream = new FileStream(physicalLocation + "/description/feladat.txt.json", FileMode.Create))
                    {
                        formFile.CopyTo(fileStream);
                    }
                }
                else if(fileType == "cpp"){
                    using (var fileStream = new FileStream(physicalLocation + "/solutions/feladat.cpp", FileMode.Create))
                    {
                        formFile.CopyTo(fileStream);
                    }
                }
            }
        }

        public void CreateTestcase2Location(String location, String testId, IFormFile formFile)
        {
            PhysicalFileProvider rootFileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            String physicalLocation = rootFileProvider.GetFileInfo("tasks").PhysicalPath + "/" + location;

            if (!rootFileProvider.GetFileInfo("tasks/" + location).Exists)
                Directory.CreateDirectory(physicalLocation  + "/tests");

            if (formFile.Length > 0)
            {
                using (var fileStream = new FileStream(physicalLocation + "/tests/" + testId, FileMode.Create))
                {
                    formFile.CopyTo(fileStream);
                }
            }
        }
    }
}


