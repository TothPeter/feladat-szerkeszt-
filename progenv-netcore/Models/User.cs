﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace AngularTaskManager.Models
{
    public class User
    {
        [Key]
        public Int32 id { get; set; }
        public String email { get; set; }
        public String auth_provider { get; set; }
        private String _auth_provider_data;
        public String auth_provider_data
        {
            get { return JsonConvert.SerializeObject(_auth_provider_data); }
            set { _auth_provider_data = JsonConvert.DeserializeObject<String>(value); }
        }
        public String auth_provider_id { get; set; }
        public String family_name { get; set; }
        public String given_name { get; set; }
        public String name { get; set; }
        public String image_url { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }
}
