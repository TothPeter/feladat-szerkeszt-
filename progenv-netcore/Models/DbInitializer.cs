﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTaskManager.Models
{
    public class DbInitializer
    {
        private static MyDbContext _context;
        public static void Initialize(MyDbContext context)
        {
            _context = context;

            _context.Database.EnsureCreated();

            if (_context.tasks.Any())
            {
                return;
            }

            SeedTasks();
            SeedLabels();
            SeedUsers();
            SeedTasksLabels();
        }

        private static void SeedTasks()
        {

            var tasks = new Task[]
            {
                new Task
                {
                    location = "000000001",
                    mester_id = "000001_000001",
                    uuid = new Guid(Encoding.ASCII.GetBytes("8cd68122-0cf1-45fd-9948-1f4c11545f9f")),
                    friendly_id = "osszeg",
                    title = "Összeg",
                    text_type = "html",
                    text = "<html>",
                    description_data = "idksomejson",
                    difficulty = 1,
                    interactive = false,
                    number_of_tests = 10,
                    number_of_subtests = 1,
                    sum_points = 100,
                    memory_limit = 32000,
                    time_limit = 0.1,
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new Task
                {
                    location = "000000002",
                    mester_id = "000001_000002",
                    uuid = new Guid(Encoding.ASCII.GetBytes("61ddafa1-5c99-4793-a464-334771f13fdd")),
                    friendly_id = "szorzat",
                    title = "Összeg",
                    text_type = "html",
                    text = "<html>",
                    description_data = "idksomejson",
                    difficulty = 1,
                    interactive = false,
                    number_of_tests = 10,
                    number_of_subtests = 1,
                    sum_points = 100,
                    memory_limit = 32000,
                    time_limit = 0.1,
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                }
            };
            foreach (Task t in tasks)
            {
                _context.tasks.Add(t);
            }

            _context.SaveChanges();
        }

        private static void SeedLabels()
        {

            var labels = new Label[]
            {
                new Label
                {
                    text = "Programozási tételek: megszámolás",
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new Label
                {
                    text = "Programozási tételek: eldöntés",
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new Label
                {
                    text = "Könnyű",
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new Label
                {
                    text = "Nehéz",
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                }
            };
            foreach (Label l in labels)
            {
                _context.labels.Add(l);
            }

            _context.SaveChanges();
        }

        private static void SeedUsers()
        {

            var users = new User[]
            {
                new User
                {
                    email = "gyozke@gmail.com",
                    auth_provider = "google",
                    auth_provider_data = "json",
                    auth_provider_id = "108543203203726484042",
                    family_name = "Horváth",
                    given_name = "Győző",
                    name = "Horváth Győző",
                    image_url = "someurl",
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                }
            };
            foreach (User u in users)
            {
                _context.users.Add(u);
            }

            _context.SaveChanges();
        }

        private static void SeedTasksLabels()
        {

            var taskslabels = new TaskLabel[]
            {
                new TaskLabel
                {
                    task_id = 1,
                    label_id = 1,
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new TaskLabel
                {
                    task_id = 1,
                    label_id = 3,
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new TaskLabel
                {
                    task_id = 2,
                    label_id = 2,
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                },
                new TaskLabel
                {
                    task_id = 2,
                    label_id = 3,
                    created_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local),
                    updated_at = new DateTime(2019, 11, 07, 9, 14, 53, DateTimeKind.Local)
                }
            };
            foreach (TaskLabel tl in taskslabels)
            {
                _context.x_tasks_labels.Add(tl);
            }

            _context.SaveChanges();
        }
    }
}
