﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AngularTaskManager.Models
{
    public class Submission
    {
        [Key]
        public Int32 id { get; set; }
        public String type { get; set; }
        public String in_data { get; set; }
        public String out_data { get; set; }
        public Int32 task_id { get; set; }
        public Int32 user_id { get; set; }
        public String created_at { get; set; }
        public String updated_at { get; set; }
    }
}
