﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AngularTaskManager.Models
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options) { }

        public DbSet<Task> tasks { get; set; }
        public DbSet<Label> labels { get; set; }
        public DbSet<TaskLabel> x_tasks_labels { get; set; }
        public DbSet<MesterMigration> mester_migrations { get; set; }
        public DbSet<Submission> submissions { get; set; }
        public DbSet<User> users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TaskLabel>()
                .HasKey(tl => new { tl.task_id, tl.label_id });
        }
    }
}

