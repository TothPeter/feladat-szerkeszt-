﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace AngularTaskManager.Models
{
    public interface IService
    {
        IEnumerable<Task> Tasks { get; }
        IEnumerable<TaskLabel> TasksLabels { get; }
        IEnumerable<Label> Labels { get; }
        IEnumerable<User> Users { get; }
        IEnumerable<DirDTO> Locations { get; }

        // Task GetTask(Task task);
        void AddTask(Task task);
        void UpdateTask(Task task);
        void RemoveTask(Task task);
        int GetTestFileNumberByLocationAndType(String location,String fileType);
        String[] GetFileNamesInSubFolder(String location, String subFolderName);
        String GetFileByLocation(String location, String fileType);
        String GetTestByLocation(String location, String testId);
        void CreateFile2Location(String location, String fileType, IFormFile formFile);
        void CreateTestcase2Location(String location, String testId, IFormFile formFile);
    }
}
