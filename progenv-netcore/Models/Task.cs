﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace AngularTaskManager.Models
{
    public class Task
    {
        [Key]
        public Int32 id { get; set; }
        public String location { get; set; }
        public String mester_id { get; set; }
        public Guid uuid { get; set; }
        public String friendly_id { get; set; }
        public String title { get; set; }
        public String text_type { get; set; }
        public String text { get; set; }
        private String _description_data;
        public String description_data
        {
            get { return JsonConvert.SerializeObject(_description_data); }
            set { _description_data = JsonConvert.DeserializeObject<String>(value); }
        }
        public Int32 difficulty { get; set; }
        public Boolean interactive { get; set; }
        public Int32 number_of_tests { get; set; }
        public Int32 number_of_subtests { get; set; }
        public Int32 sum_points { get; set; }
        public Int32 memory_limit { get; set; }
        public Double time_limit { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }

        public override bool Equals(Object obj)
   {
       Console.Write("Checking equality.");
      //Check for null and compare run-time types.
      if ((obj == null) || ! this.GetType().Equals(obj.GetType())) 
      {
         return false;
      }
      else { 
         Task t = (Task) obj; 
         return (location.Equals( t.location));
      }   
   }

   public override int GetHashCode()
   {
      int hash = 17;
        hash = hash * 23 + location.GetHashCode();
        return hash;
   }
    }
}

