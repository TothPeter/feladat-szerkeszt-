using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AngularTaskManager.Models;

namespace AngularTaskManager.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FileController : ControllerBase
    {
        private readonly IService _service;

        public FileController(IService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_service.Locations.ToArray());
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{location}")]
        public IActionResult GetDir(String location)
        {
            try
            {
                return Ok(_service.Locations.Where(dir => dir.location == location));
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
[HttpGet("{location}/test/count/{fileType}")]
public IActionResult GetNumOfTestFilesFromType(String location, String fileType){
         try
            {
                int numberOfFiles = _service.GetTestFileNumberByLocationAndType(location, fileType);
                if (numberOfFiles != -1)
                {
                    return Ok(numberOfFiles);
                }

                return NotFound();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
}
[HttpGet("{location}/fold/{subFolder}")]
public IActionResult GetFileNamesInFolder(String location, String subFolder){
    //Console.Write(".................................................");
    try{
    string[] foundFiles=_service.GetFileNamesInSubFolder(location,subFolder);
    if(foundFiles!=null){
        return Ok(foundFiles);
    }
    return NotFound();
    }
    catch
    {
        return StatusCode(StatusCodes.Status500InternalServerError);
    }
}
        [HttpGet("{location}/{fileType}")]
        public IActionResult GetFile(String location, String fileType)
        {
            try
            {
                String fileContent = _service.GetFileByLocation(location, fileType);
                if (fileContent != "")
                {
                    return Ok(fileContent);
                }

                return NotFound();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{location}/{test}/{testId}")]
        public IActionResult GetTest(String location, String test, String testId)
        {
            try
            {
                if (test == "test")
                {
                    String fileContent = _service.GetTestByLocation(location, testId);
                    if (fileContent != "")
                    {
                        return Ok(fileContent);
                    }

                    return NotFound();
                }
                
                return BadRequest();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost("{location}/{fileType}")]
        public ActionResult PostFile(String location, String fileType)
        {
            try
            {
                _service.CreateFile2Location(location, fileType, Request.Form.Files[0]);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost("{location}/{test}/{testId}")]
        public IActionResult PostTest(String location, String test, String testId)
        {
            try
            {
                // create a new testcase with this content
                if (test == "test")
                {
                    _service.CreateTestcase2Location(location, testId, Request.Form.Files[0]);
                    return Ok();
                }

                return BadRequest();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
