﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AngularTaskManager.Models;

namespace AngularTaskManager.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LabelController : ControllerBase
    {
        private readonly IService _service;

        public LabelController(IService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_service.Labels.ToArray());
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{search}")]
        public IActionResult GetLabelsByText(String search, [FromBody] String searchText)
        {
            try
            {
                if (search == "search")
                {
                    Console.WriteLine("WWWWWWW: "+ searchText);
                    return Ok(_service.Labels.Where(label => label.text.Contains(searchText)).ToArray());
                }

                return NotFound();
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
