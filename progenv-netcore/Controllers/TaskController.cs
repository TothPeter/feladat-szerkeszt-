﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AngularTaskManager.Models;

namespace AngularTaskManager.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly IService _service;

        public TaskController(IService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_service.Tasks.ToArray());
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("location={queryPath}")]
        public IActionResult GetTask(String queryPath)
        {
            try
            {
                return Ok(_service.Tasks.Where(task => task.id == Int32.Parse(queryPath))
                    .Select(task => new TaskDTO {
                        id = task.id,
                        friendly_id = task.friendly_id,
                        title = task.title,
                        difficulty = task.difficulty,
                        memory_limit = task.memory_limit,
                        time_limit = task.time_limit
                    }).ToArray());
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("searchtype={taskType}&searchtext={taskText}")]
        public IActionResult GetSearchedTypeText(String taskType, String taskText)
        {
            try
            {
                Console.WriteLine("text: "+ taskText + " type: " + taskType);
                
                if (taskText == "nosearchtext") { taskText = ""; }
                if (taskType == "nosearchtype") { taskType = ""; }

                var taskIds = _service.Tasks.Where(t => t.text.Contains(taskText)).Select(t => t.id);
                var labelIds = _service.Labels.Where(l => l.text.Contains(taskType)).Select(l => l.id);
                
                HashSet<Task> resultTasks = new HashSet<Task>();
                var tasklabels = _service.TasksLabels.ToList();

                foreach (TaskLabel tl in tasklabels)
                {
                    if (labelIds.Contains(tl.label_id) && taskIds.Contains(tl.task_id))
                    {
                        resultTasks.Add(_service.Tasks.FirstOrDefault(t => t.id == tl.task_id));
                    }
                }

                return Ok(resultTasks.ToArray());
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public IActionResult PostTask([FromBody] TaskDTO taskDTO)
        {
            // try
            // {
            //     var addedTask = _service.Tasks.Add(new Task
            //     {
            //         // taskDTO to task
            //     });
            // }
            // catch
            // {
                return StatusCode(StatusCodes.Status500InternalServerError);
            // }
        }

        public IActionResult PutTask([FromBody] TaskDTO taskDTO)
        {
            try
            {
                Task task = _service.Tasks.FirstOrDefault(task => task.id == taskDTO.id);
                if (task == null) return NotFound();
                
                // task.time_limit = taskDTO.time_limit;
                // _context.SaveChanges();
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTask(Int32 id)
        {
            try
            {
                Task task = _service.Tasks.FirstOrDefault(task => task.id == id);
                if (task == null) return NotFound();

                _service.RemoveTask(task);
                return Ok();
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
