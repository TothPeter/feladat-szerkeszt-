﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AngularTaskManager.Models;

namespace AngularTaskManager.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TaskLabelController : ControllerBase
    {
        private readonly IService _service;

        public TaskLabelController(IService service)
        {
            _service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(_service.TasksLabels.ToArray());
            }
            catch
            {
                // Internal Server Error
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{labelId}")]
        public IActionResult GetTasksLabelsByLabelId(Int32 labelId)
        {
            try
            {
                return Ok(_service.TasksLabels.Where(tl => tl.label_id == labelId).ToArray());
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
